use std::env;

/**
 * Take the backup number and tapes and return the index of the tape to return.
 *
 * Will panic if "tapes" is 0.
 */
fn hanoi(backup: u64, tapes: u32) -> u32 {
    if tapes == 0 {
        panic!("Can not call hanoi with a 0 tapes argument");
    }

    // Find latest tape whose power-of-two frequency matches this number.
    // Should never panic.
    (0..tapes)
        .rev()
        .find(|&i| backup % 2u64.pow(i) == 0)
        .unwrap()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut args: Vec<String> = env::args().skip(1).collect();
    if args.len() <= 2 {
        return Err(String::from(
            "Need a number for the current backup number, and a list of tapes as the rest of the arguments",
        ).into());
    }

    let backup: u64 = args.remove(0).parse()?;

    let tapes = args;

    let index = hanoi(
        backup,
        u32::try_from(tapes.len()).expect("tape count can not exceed a u32"),
    );

    println!("{}", tapes[usize::try_from(index).unwrap()]);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::hanoi;
    use std::iter;
    const CYCLE_SIZE: usize = 10000;

    #[test]
    fn one_tape() {
        let output: Vec<_> = (0..CYCLE_SIZE).map(|i| hanoi(i as u64, 1)).collect();
        let check: Vec<_> = iter::repeat(0).take(CYCLE_SIZE).collect();
        assert_eq!(output, check);
    }

    #[test]
    fn two_tape() {
        let output: Vec<_> = (0..CYCLE_SIZE).map(|i| hanoi(i as u64, 2)).collect();
        let check: Vec<_> = vec![1, 0].into_iter().cycle().take(CYCLE_SIZE).collect();
        assert_eq!(output, check);
    }

    #[test]
    fn three_tape() {
        let output: Vec<_> = (0..CYCLE_SIZE).map(|i| hanoi(i as u64, 3)).collect();
        let check: Vec<_> = vec![2, 0, 1, 0]
            .into_iter()
            .cycle()
            .take(CYCLE_SIZE)
            .collect();
        assert_eq!(output, check);
    }

    #[test]
    fn four_tape() {
        let output: Vec<_> = (0..CYCLE_SIZE).map(|i| hanoi(i as u64, 4)).collect();
        let check: Vec<_> = vec![3, 0, 1, 0, 2, 0, 1, 0]
            .into_iter()
            .cycle()
            .take(CYCLE_SIZE)
            .collect();
        assert_eq!(output, check);
    }

    #[test]
    fn five_tape() {
        let output: Vec<_> = (0..CYCLE_SIZE).map(|i| hanoi(i as u64, 5)).collect();
        let check: Vec<_> = vec![4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0]
            .into_iter()
            .cycle()
            .take(CYCLE_SIZE)
            .collect();
        assert_eq!(output, check);
    }
}
