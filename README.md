# hanoirotation

A simple command line application to calculate a hanoi rotation tape.

# usage

```sh
$ hanoirotation {backupnumber} {tape} ...
{tape}
```

# example

```sh
$ for i in {0..32}; do ./target/debug/hanoirotation $i A B C D E; done
E
A
B
A
C
A
B
A
D
A
B
A
C
A
B
A
E
A
B
A
C
A
B
A
D
A
B
A
C
A
B
A
E
```
